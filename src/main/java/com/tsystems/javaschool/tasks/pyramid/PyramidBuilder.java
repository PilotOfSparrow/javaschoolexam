package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        try {
            int numListSize = inputNumbers.size();

            // any triangular number should give perfect square
            if (Math.sqrt(numListSize * 8 + 1) % 1 != 0) throw new CannotBuildPyramidException();

            int rowsAmount = countRows(numListSize);
            int columnsAmount = rowsAmount + (rowsAmount - 1);
            int[][] pyramid = new int[rowsAmount][columnsAmount];

            Collections.sort(inputNumbers);

            int middle     = (columnsAmount - 1) / 2;
            int valueIndex = 0;  // index of element been taken from input array
            for (int row = 0; row < rowsAmount; ++row) {
                int insertionsCounter = 0;
                int insertPoint       = middle - row;
                while (insertionsCounter != (row + 1)) {
                    pyramid[row][insertPoint] = inputNumbers.get(valueIndex);

                    ++insertionsCounter;
                    ++valueIndex;
                    insertPoint += 2;
                }
            }

            return pyramid;

        } catch (NullPointerException e) {
            throw new CannotBuildPyramidException();
        }
    }

    private int countRows(int n) {
        return (int)Math.round(((Math.sqrt(1 + 8 * n) - 1) / 2));
    }

}
