package com.tsystems.javaschool.tasks.calculator;

import java.util.*;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here
        if (statement == null || statement.equals("")) return null;

        List<String> input = parse(statement);

        if (input == null || input.size() < 3) return null;

        Deque<Double> stack = new LinkedList<>();
        for (String s : input) {
            switch (s) {
                case "+":
                    stack.push(stack.pop() + stack.pop());
                    break;
                case "-":
                    Double minuend    = stack.pop();
                    Double subtrahend = stack.pop();
                    stack.push(subtrahend - minuend);
                    break;
                case "*":
                    stack.push(stack.pop() * stack.pop());
                    break;
                case "/":
                    Double divisor  = stack.pop();
                    Double dividend = stack.pop();

                    if (divisor.equals(0.0)) return null;

                    stack.push(dividend / divisor);
                    break;
                default:
                    stack.push(Double.valueOf(s));
                    break;
            }
        }

        if (stack.size() != 1) return null;

        if (stack.peek() % 1 == 0) {
            return String.valueOf((int) stack.pop().doubleValue());
        } else {
            return String.format("%.4f", stack.pop()).replaceAll("[^.1-9]", "");
        }
    }

    private enum Operator {
        ADD(1), SUB(2), MULT(3), DIV(4);

        final int precedence;

        Operator(int p) {
            precedence = p;
        }
    }

    private Map<Character, Operator> operators = new HashMap<Character, Operator>() {{
        put('+', Operator.ADD);
        put('-', Operator.SUB);
        put('*', Operator.MULT);
        put('/', Operator.DIV);
    }};

    private boolean isHigerPrec(char op, char sub) {
        return (operators.containsKey(sub)
                && operators.get(sub).precedence >= operators.get(op).precedence);
    }

    // Shunting-yard algorithm
    private List<String> parse(String statement) {
        if (statement == null) return null;

        List<String>     splitedStatement = new ArrayList<>();
        Deque<Character> operatorsStack   = new LinkedList<>();
        StringBuilder    numStringBuilder = new StringBuilder();

        int operatorsCounter = 0; // for checking correlation between amount of numbers and operators
        int numbersCounter   = 0; // numbersCounter - operatorsCounter should always be 1
        int bracketsCouter   = 0; // should be zero

        boolean isNumPresented = false; // number should start, well, with number
        boolean isNumHaveDot   = false; // number can contain only one dot

        for (int i = 0; i != statement.length(); ++i) {
            char curr = statement.charAt(i);

            // Number
            if (Character.isDigit(curr)) {
                numStringBuilder.append(curr);

                isNumPresented = true;

            } else if (curr == '.') {
                if (!isNumPresented || isNumHaveDot) return null;
                numStringBuilder.append(curr);

                isNumHaveDot = true;

            } else if (numStringBuilder.length() != 0) {
                splitedStatement.add(numStringBuilder.toString());
                numStringBuilder = new StringBuilder();

                ++numbersCounter;

                isNumHaveDot   = false;
                isNumPresented = false;
            }

            // Operator
            if (operators.containsKey(curr)) {
                while (!operatorsStack.isEmpty() && isHigerPrec(curr, operatorsStack.peek()))
                    splitedStatement.add(String.valueOf(operatorsStack.pop()));
                operatorsStack.push(curr);

                ++operatorsCounter;

            // Brackets
            } else if (curr == '(') {
                operatorsStack.push(curr);

                ++bracketsCouter;

            } else if (curr == ')') {
                while (operatorsStack.peek() != '(') {
                    splitedStatement.add(String.valueOf(operatorsStack.pop()));

                    if (operatorsStack.isEmpty()) return null;
                }

                operatorsStack.pop();

                --bracketsCouter;
            }
        }

        if (numStringBuilder.length() != 0) {
            splitedStatement.add(numStringBuilder.toString());

            ++numbersCounter;
        }

        if ((numbersCounter - operatorsCounter) != 1) return null;
        if (bracketsCouter != 0) return null;

        while (!operatorsStack.isEmpty())
            splitedStatement.add(String.valueOf(operatorsStack.pop()));

        return splitedStatement;
    }
}
