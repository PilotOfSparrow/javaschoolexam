package com.tsystems.javaschool.tasks.subsequence;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null || y == null) throw new IllegalArgumentException();
        if (x.isEmpty() && y.isEmpty()) return true;

        // n pow 3 on memory in big O, like a bad boy
        HashSet xSet = new HashSet<>(x);
        HashSet yOccurrencesSet = new HashSet();
        List tmp = new ArrayList();
        for (Object obj : y) {
            if (xSet.contains(obj) && !yOccurrencesSet.contains(obj)) {
                tmp.add(obj);
                yOccurrencesSet.add(obj);
            }
        }

        return x.equals(tmp);
    }
}
